# AWS Lambda
AWS Lambda function that swap message participants

## Getting Started
These instructions tells how to deploy the function to your AWS account

## Prerequisites
Node.JS 10+ (https://nodejs.org/en/download/)
AWS account

## Installing and configuring serverless pakcage
(full guide: https://stackify.com/aws-lambda-with-node-js-a-complete-getting-started-guide/)
1. Install globally serverless npm package running ```npm i -g serverless```
2. Login to AWS console and go to IAM user
3. Click on Add user to begin the user creation process
4. Type the user name, enable programmatic access by checking the checkbox, and click on Next: permissions to proceed.
5. Click on Attach existing policy directly and search for Administrator access. Select AdministratorAccess by checking the box.
6. Review your choices and then click on the Create user button.
7. Copy or download a CSV file containing your access key ID and access key secret. You need to keep this secure. 
8. Configure serverless CLI with your AWS credentials. This is necessary for deployment.  
```serverless config credentials --provider aws --key <your_access_key_id> --secret <your_access_key_secret>```

## Deployment
1. Clone and open the project folder in the command line
2. Run ```serverless deploy```
3. Wait and then you'll see the link to your lambda

## Testing
Send POST request to the lambda URL with content type 'application/xml'.
Body example:
```
<?xml version="1.0" encoding="UTF-8"?>
<message>
    <to>Chirag</to>
    <from>Jay</from>
    <body>How are you?</body>
</message>
```
