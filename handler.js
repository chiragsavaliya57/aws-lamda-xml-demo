'use strict';

const xml2js = require('xml2js');
const Joi = require('joi');

const xmlBuilder = new xml2js.Builder();
const dataSchema = Joi.object().keys({
  message: Joi.object().keys({
    from: Joi.string().required(),
    to: Joi.string().required(),
    body: Joi.string()
  }).required()
});

module.exports.helloWorld = (event, context, callback) => {
  parseXml(event.body, (err, data) => {
    if (err) {
      return callback(null, createErrorResponse(err));
    }

    validateData(data, (err) => {
      if (err) {
        return callback(null, createErrorResponse(err));
      }
      
      const responseData = {
        message: swapMessageParticipants(data.message)
      }
      const responseDataXml = buildXml(responseData);
  
      callback(null, createSuccessResponse(responseDataXml));
    });
  })
};

function parseXml(xml, callback) {
  xml2js.parseString(
    xml,
    {
      explicitArray: false
    },
    callback
  );
}

function buildXml(data) {
  return xmlBuilder.buildObject(data);
}

function swapMessageParticipants(message) {
  return {
    ...message,
    from: message.to,
    to: message.from
  }
}

function validateData(data, callback) {
  dataSchema.validate(data, callback);
}

function createSuccessResponse(data) {
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/xml'
    },
    body: data
  };
}

function createErrorResponse(err) {
  return {
    statusCode: 500,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(err)
  };
}